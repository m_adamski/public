PRAGMA foreign_keys=OFF;

BEGIN TRANSACTION;

CREATE TABLE feeders
(
    position        INTEGER PRIMARY KEY ON CONFLICT REPLACE,
    name            TEXT NOT NULL,
    serial          TEXT,
    description     TEXT,
    used_by_gateway TEXT,
    status          INTEGER DEFAULT 0
);

CREATE TABLE phases
(
    position       INTEGER NOT NULL,
    feeder         INTEGER,
    name           TEXT NOT NULL,
    PRIMARY KEY(position, feeder) ON CONFLICT REPLACE
    FOREIGN KEY(feeder) REFERENCES feeders(position) ON DELETE CASCADE
);

CREATE TABLE installation
(
    id                  VARCHAR(38) PRIMARY KEY, -- TODO: add check constraint for UUID formatting
    area_id             VARCHAR(38),
    area_name           TEXT NOT NULL,
    location_id         VARCHAR(38),
    location_name       TEXT NOT NULL,
    location_ref_no     TEXT,
    location_address    TEXT,
    location_descr      TEXT DEFAULT '',
    location_type       TEXT NOT NULL CHECK (location_type IN ('FUSE_PILLAR', 'SIMPLE_LINK_BOX')),
    install_time        TEXT NOT NULL,
    uninstall_time      TEXT DEFAULT NULL,   -- there is only one row with this set to not null
    state               TEXT DEFAULT 'CREATE_LOCATION'
);

CREATE TABLE area_cache
(
    id          VARCHAR(38) PRIMARY KEY, -- TODO: add check constraint for UUID formatting
    parent      VARCHAR(38) REFERENCES area_cache(id),
    name        TEXT,
    description TEXT DEFAULT '',
    etag_local  TEXT DEFAULT '',
    etag_server TEXT DEFAULT ''
);

INSERT INTO "area_cache" VALUES('42a8bdc8-b5bd-11e3-a670-940c6d8403a1',NULL,'Entire Network','','','');

INSERT INTO "area_cache" VALUES('2a5b01ab-ba68-11e3-a670-940c6d8403a1','42a8bdc8-b5bd-11e3-a670-940c6d8403a1','dummy','','','');

INSERT INTO "area_cache" VALUES('49a2ed74-ba68-11e3-a670-940c6d8403a1','42a8bdc8-b5bd-11e3-a670-940c6d8403a1','QUARANTINE','','','');

CREATE TABLE area_cache_etag
(
    etag_local  TEXT DEFAULT '' -- TODO: there can be only one row in this table
);

INSERT INTO "area_cache_etag" VALUES('');

CREATE TABLE location_cache
(
    id           VARCHAR(38) PRIMARY KEY, -- TODO: add check constraint for UUID formatting
    area_id      VARCHAR(38),
    reference_no TEXT,
    name         TEXT,
    address      TEXT,
    description  TEXT DEFAULT '',
    type         TEXT NOT NULL,
    FOREIGN KEY(area_id) REFERENCES area_cache(id) ON DELETE CASCADE
);

CREATE TABLE feeders_cache
(
    location_id     VARCHAR(38),
    position        INTEGER NOT NULL,
    name            TEXT NOT NULL,
    serial          TEXT,
    description     TEXT,
    used_by_gateway TEXT,
    PRIMARY KEY(location_id, position) ON CONFLICT REPLACE,
    FOREIGN KEY(location_id) REFERENCES location_cache(id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE phases_cache
(
    location_id    VARCHAR(38),
    position       INTEGER NOT NULL,
    feeder         INTEGER,
    name           TEXT NOT NULL,
    PRIMARY KEY(location_id, position, feeder) ON CONFLICT REPLACE,
    FOREIGN KEY(location_id) REFERENCES location_cache(id) ON DELETE CASCADE,
    FOREIGN KEY(feeder, location_id) REFERENCES feeders_cache(position, location_id) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE phone_number (
    id INTEGER PRIMARY KEY,
    number varchar(20)
);

CREATE TABLE FilesForUpload (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    priority int,
    request_timestamp int,
    file_name varchar(80),
    sent_file_timestamp int,
    file_status int,
    file_size int,
    file_type int,
    file_revision int,
    aggregation_type int
);

CREATE TABLE CDFilesToDownload (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    url_to_file varchar(200),
    path_to_file varchar(200),
    device_serial varchar(17),
    priority int,
    request_timestamp varchar(17),
    file_count_on_device int,
    timeout int,
    transfer_id int,
    error_counter int
);

CREATE TABLE tarFile (
    path VARCHAR(64)
);

CREATE TABLE RODReason
(
    reason          int PRIMARY KEY ON CONFLICT REPLACE,
    startTime       int
);

CREATE TABLE status
(
    serial_num          varchar(17),
    name                varchar(20),
    type                varchar(10),
    value               varchar(40) DEFAULT NULL,
    dev_time            int DEFAULT 0,
    dev_time_msec       int DEFAULT 0,
    UNIQUE              (serial_num, name) ON CONFLICT REPLACE,
    FOREIGN KEY(serial_num) REFERENCES device(serial_num) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE class_assignments
(
  point_type   TEXT NOT NULL,
  point_index  INTEGER NOT NULL,
  class_mask   INTEGER NOT NULL,
  UNIQUE       (point_type, point_index) ON CONFLICT REPLACE
);

CREATE TABLE schema_version
(
    patch_name          TEXT NOT NULL,
    application_time    INTEGER,
    UNIQUE (patch_name) ON CONFLICT ROLLBACK
);

INSERT INTO "schema_version" VALUES('001_device_config_checksum.sql','2016-09-26 13:48:00');

INSERT INTO "schema_version" VALUES('002_retrospective_operations.sql','2016-09-26 13:48:00');

INSERT INTO "schema_version" VALUES('003_rapid_mode_allow_device_installation_without_created_feeders.sql','2016-09-26 13:48:01');

INSERT INTO "schema_version" VALUES('004_remote_operations.sql','2016-09-26 13:48:01');

INSERT INTO "schema_version" VALUES('005_identify_operation.sql','2016-09-26 13:48:01');

INSERT INTO "schema_version" VALUES('006_device_operations_history.sql','2016-09-26 13:48:01');

CREATE TABLE "device"
(
    serial_num          varchar(17)  PRIMARY KEY ON CONFLICT REPLACE,
    install_status      int DEFAULT 0,
    cfg_checksum        varchar(40) DEFAULT ""
);

CREATE TABLE retrospective_installation_requests
(
    operation_id INTEGER,
    request_time INTEGER NOT NULL,
    PRIMARY KEY (operation_id) ON CONFLICT ABORT
);

CREATE TABLE retrospective_installation_changes
(
    operation_id INTEGER,
    change_id    INTEGER, 
    change_type  TEXT NOT NULL,
    status       TEXT NOT NULL 
        DEFAULT 'WAIT_FOR_CONFIRMATION' 
        CHECK (status IN ('WAIT_FOR_CONFIRMATION', 'MANUAL_ACCEPTED', 'MANUAL_REJECTED',
                          'AUTO_ACCEPTED', 'AUTO_REJECTED', 'OUTDATED', 'GATEWAY_ERROR')),
    timestamp    INTEGER NOT NULL,
    PRIMARY KEY (change_id) ON CONFLICT ABORT,
    FOREIGN KEY (operation_id) REFERENCES retrospective_installation_requests(operation_id) ON DELETE CASCADE
);

CREATE TABLE retrospective_installation_data
(
    change_id    INTEGER,
    name         TEXT,
    value        TEXT,
    FOREIGN KEY (change_id) REFERENCES retrospective_installation_changes(change_id) ON DELETE CASCADE
);

CREATE TABLE "device_installation"
(
    serial_num          varchar(17),
    feeder_position     INTEGER,
    phase_position      INTEGER NOT NULL,
    PRIMARY KEY(serial_num, feeder_position, phase_position) ON CONFLICT REPLACE,
    FOREIGN KEY(serial_num) REFERENCES device(serial_num) ON DELETE CASCADE DEFERRABLE INITIALLY DEFERRED
);

CREATE TABLE "remote_operation"
(
    id                  INTEGER PRIMARY KEY ON CONFLICT IGNORE,
    serial_num          varchar(17) REFERENCES device(serial_num) ON DELETE CASCADE,
    type                text not null check (type IN ('RESET_TTL', 'CLOSE', 'FAULT_THUMP', 'TRIP', 'SET_CONFIG', 'GET_CONFIG', 'SET_SINGLE_CONFIGURATION_VALUE', 'CLEAR_INTERNAL_ERRORS', 'IDENTIFY')),
    status              text not null default 'NEW' check (status IN ('NEW', 'STARTED')),
    timeout             INTEGER default 2000,   -- milliseconds
    UNIQUE (serial_num, type) ON CONFLICT ABORT,
    UNIQUE (serial_num, status) ON CONFLICT ABORT
);

CREATE TABLE device_operations_history
(
    id                  INTEGER PRIMARY KEY AUTOINCREMENT,
    serial_num          varchar(17) REFERENCES device(serial_num) ON DELETE CASCADE,
    type                text not null check (type IN ('CLOSE', 'TRIP')),
    subtype             INTEGER,
    source              INTEGER,
    feeder_current      INTEGER,
    timestamp           text not null
);

CREATE TRIGGER installation_one AFTER INSERT ON installation 
BEGIN
    UPDATE installation SET uninstall_time = datetime('now') WHERE uninstall_time IS NULL and not id=new.id;
END;

CREATE TRIGGER tarFile_one_row_limit BEFORE INSERT ON tarFile
BEGIN
    DELETE FROM tarFile;
END;

CREATE TRIGGER device_operations_history_limit AFTER INSERT ON device_operations_history
BEGIN DELETE FROM device_operations_history where serial_num=NEW.serial_num AND id not in (SELECT id FROM device_operations_history WHERE serial_num=NEW.serial_num ORDER BY id DESC LIMIT 3);
 END;

COMMIT;
